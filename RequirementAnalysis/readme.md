Phân công:
Trịnh Quang Anh - Phần 1: Đăng ký mượn sách, Xem danh sách mượn sách, Huỷ đăng ký mượn sách.
Phạm Thế Anh - Phần 2: Cho mượn sách, Tìm kiếm thông tin mượn, Thêm bản sao.
Trần Tuấn Anh - Phần 3: Nhận trả sách, Tìm kiếm thông tin trả, Thêm sách.
Nguyễn Tuấn Anh - Phần 4: Tìm kiếm sách, Phát hành thẻ bạn đọc, Cập nhật thông tin thẻ bạn đọc.