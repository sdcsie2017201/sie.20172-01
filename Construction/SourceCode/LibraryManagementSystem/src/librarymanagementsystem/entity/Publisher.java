/**
 * Tran Tuan Anh
 * Date: May 8, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import librarymanagementsystem.utils.DBUtils;

/**
 * This class use to work with the Publisher table in database.
 * @author gadfl
 *
 */
public class Publisher extends DBUtils {

	private final String ADD_PUBLISHER = "INSERT INTO Publisher(publisherName) VALUES(?)";
	private final String GET_ALL_PUBLISHER = "SELECT * FROM Publisher";
	private final String SEARCH_PUBLISHER = "SELECT * FROM Publisher WHERE publisherName LIKE ?";
	
	private String publisherName;

	/**
	 * @return the publisherName
	 */
	public String getPublisherName() {
		return publisherName;
	}

	/**
	 * @param publisherName the publisherName to set
	 */
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}
	
	/**
	 * Add new publisher.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void add() throws ClassNotFoundException, SQLException {
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(ADD_PUBLISHER);
		stmt.setString(1, publisherName);
		stmt.execute();
		closeConnection();
	}
	
	/**
	 * Get all the publisher name from database.
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public String[] getAllPublisher() throws ClassNotFoundException, SQLException {
		String publisherName = new String();
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(GET_ALL_PUBLISHER);
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			publisherName += rs.getString("publisherName")+ ",";
		}
		closeConnection();
		return publisherName.split(",");
	}
	
	/**
	 * Check if the publisher is existed in the database.
	 * @param publisherName
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean checkExistPublisher(String publisherName) throws ClassNotFoundException, SQLException {
		boolean exist = false;
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(SEARCH_PUBLISHER);
		stmt.setString(1, "%"+publisherName+"%");
		ResultSet rs = stmt.executeQuery();
		if(rs.next()) {
			exist = true;
		}
		closeConnection();
		return exist;
	}
	
}
