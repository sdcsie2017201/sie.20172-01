/**
0 * Tran Tuan Anh
 * Date: Apr 2, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import librarymanagementsystem.utils.DBUtils;

/**
 * This class is used to manage the book in database
 * 
 * @author gadfl
 *
 */
public class Book extends DBUtils {

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookAuthor == null) ? 0 : bookAuthor.hashCode());
		result = prime * result + ((bookCategory == null) ? 0 : bookCategory.hashCode());
		result = prime * result + bookID;
		result = prime * result + ((bookPublisher == null) ? 0 : bookPublisher.hashCode());
		result = prime * result + ((bookTitle == null) ? 0 : bookTitle.hashCode());
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (bookAuthor == null) {
			if (other.bookAuthor != null)
				return false;
		} else if (!bookAuthor.equals(other.bookAuthor))
			return false;
		if (bookCategory == null) {
			if (other.bookCategory != null)
				return false;
		} else if (!bookCategory.equals(other.bookCategory))
			return false;
		if (bookID != other.bookID)
			return false;
		if (bookPublisher == null) {
			if (other.bookPublisher != null)
				return false;
		} else if (!bookPublisher.equals(other.bookPublisher))
			return false;
		if (bookTitle == null) {
			if (other.bookTitle != null)
				return false;
		} else if (!bookTitle.equals(other.bookTitle))
			return false;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		return true;
	}

	private static final String ADD_BOOK = "INSERT INTO Book VALUES (?,?,?,?,?,?)";
	private static final String GET_BOOK = "SELECT * FROM Book WHERE BookID = ?";
	private static final String GET_ALL_BOOK = "SELECT * FROM Book";
	private static final String SEARCH_BOOK = "SELECT * FROM Book WHERE Title LIKE ?";

	private int bookID;
	private String bookCategory;
	private String bookTitle;
	private String bookPublisher;
	private String bookAuthor;
	private	String isbn;

	/**
	 * @param bookID the bookID to set
	 */
	public void setBookID(int bookID) {
		this.bookID = bookID;
	}

	/**
	 * @return the bookID
	 */
	public int getBookID() {
		return bookID;
	}

	/**
	 * @return the bookTitle
	 */
	public String getBookTitle() {
		return bookTitle;
	}

	/**
	 * @param bookTitle
	 *            the bookTitle to set
	 */
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	/**
	 * @return the bookPublisher
	 */
	public String getBookPublisher() {
		return bookPublisher;
	}

	/**
	 * @param bookPublisher
	 *            the bookPublisher to set
	 */
	public void setBookPublisher(String bookPublisher) {
		this.bookPublisher = bookPublisher;
	}

	/**
	 * @return the bookAuthor
	 */
	public String getBookAuthor() {
		return bookAuthor;
	}

	/**
	 * @param bookAuthor
	 *            the bookAuthor to set
	 */
	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}

	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}

	/**
	 * @param isbn
	 *            the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	/**
	 * @return the bookCategory
	 */
	public String getBookCategory() {
		return bookCategory;
	}

	/**
	 * @param bookCategory the bookCategory to set
	 */
	public void setBookCategory(String bookCategory) {
		this.bookCategory = bookCategory;
	}
	
	/**
	 * This function is used to save the book to database.
	 * 
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	public void addBook() throws SQLException, ClassNotFoundException {
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(ADD_BOOK);
		bookID = this.generateBookID();
		stmt.setInt(1, bookID);
		stmt.setString(2, bookTitle);
		stmt.setString(3, bookCategory);
		stmt.setString(4, bookAuthor);
		stmt.setString(5, bookPublisher);
		stmt.setString(6, isbn);
		if(!stmt.execute()) {
			JOptionPane.showMessageDialog(null, "Book added successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
		}else {
			JOptionPane.showMessageDialog(null, "Failed to add new book.", "Error", JOptionPane.ERROR_MESSAGE);
		}
		closeConnection();
	}

	/**
	 * Get the book information from database.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void getInformation() throws ClassNotFoundException, SQLException {
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(GET_BOOK);
		stmt.setInt(1, this.bookID);
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			this.bookTitle = rs.getString("Title");
			this.bookCategory = rs.getString("Category");
			this.bookAuthor = rs.getString("Author");
			this.bookPublisher = rs.getString("Publisher");
			this.isbn = rs.getString("ISBN");
		}
	}
	
	/**
	 * Search a book by title or author.
	 * @param title
	 * @param author
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<Book> searchBook(String title, String author) throws ClassNotFoundException, SQLException{
		ArrayList<Book> bookList = new ArrayList<>();
		String queryForAuthor = " AND Author LIKE ?";
		String query = SEARCH_BOOK;
		if(!author.isEmpty()) {
			query+= queryForAuthor;
		}
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, "%"+title+"%");
		if(!author.isEmpty()) {
			stmt.setString(2, "%"+author+"%");
		}
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			Book book = new Book();
			book.setBookID(rs.getInt("BookID"));
			book.setBookAuthor(rs.getString("Author"));
			book.setBookCategory(rs.getString("Category"));
			book.setBookPublisher(rs.getString("Publisher"));
			book.setBookTitle(rs.getString("Title"));
			book.setIsbn(rs.getString("ISBN"));
			bookList.add(book);
		}
		return bookList;
	}
	
	/**
	 * Get all the book in database.
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<Book> getAllBook() throws ClassNotFoundException, SQLException{
		ArrayList<Book> bookList = new ArrayList<>();
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(GET_ALL_BOOK);
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			Book book = new Book();
			book.setBookID(rs.getInt("BookID"));
			book.setBookAuthor(rs.getString("Author"));
			book.setBookCategory(rs.getString("Category"));
			book.setBookPublisher(rs.getString("Publisher"));
			book.setBookTitle(rs.getString("Title"));
			book.setIsbn(rs.getString("ISBN"));
			bookList.add(book);
		}
		return bookList;
	}
	
	/**
	 * This function is used to generate new book id.
	 * 
	 * @return the last book ID added by one.
	 * @throws SQLException
	 */
	private int generateBookID() throws SQLException, ClassNotFoundException {
		return this.countRecord("BOOK") + 1;
	}

}
