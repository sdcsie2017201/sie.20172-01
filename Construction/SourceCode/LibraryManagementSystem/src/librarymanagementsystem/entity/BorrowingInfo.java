/**
 * Tran Tuan Anh
 * Date: Apr 3, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.entity;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import librarymanagementsystem.utils.DBUtils;

/**
 * This class is used to manage the borrowing infomation.
 * 
 * @author gadfl
 *
 */
public class BorrowingInfo extends DBUtils {

	private final String ADD_BORROW_INFO = "INSERT INTO BorrowingInfo VALUES (?,?,?,?,?,?,?,?,?,?)";
	private final String SEARCH_BORROW_INFO = "SELECT * FROM BorrowingInfo WHERE borrowername LIKE ?";
	private final String UPDATE_INFO = "UPDATE BorrowingInfo SET bookid =?, copyid =?, compensation =?, borrowername=?, booktitle =?, borroweddate=?, lentdate=?, returndate=? , returned = ? WHERE infoid =?";
	private final String COUNT_NOT_RETURNED_INFO = "SELECT COUNT(*) AS count FROM (SELECT * FROM BorrowingInfo WHERE borrowername LIKE ? AND NOT returned)";

	private int infoID;
	private int bookID;
	private int copyID;
	private double compensation;
	private String borrowerName;
	private String bookTitle;
	private Date borrowedDate;
	private Date lentDate;
	private Date returnDate;
	private boolean returned;

	/**
	 * This method is used for add new borrowing info into database.
	 * 
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void addBorrowingInfo() throws SQLException, ClassNotFoundException {
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(ADD_BORROW_INFO);
		stmt.setInt(1, generateID());
		stmt.setInt(2, bookID);
		stmt.setInt(3, copyID);
		stmt.setDouble(4, compensation);
		stmt.setString(5, borrowerName);
		stmt.setString(6, bookTitle);
		stmt.setDate(7, borrowedDate);
		stmt.setDate(8, lentDate);
		stmt.setDate(9, returnDate);
		stmt.setBoolean(10, returned);
		stmt.execute();
		closeConnection();
	}

	/**
	 * This method is used to search borrowing information by borrowerName or
	 * bookTile.
	 * 
	 * @param borrowerName
	 *            name of the borrower.
	 * @param bookTitle
	 *            title of the book.
	 * @return a list of borrowing information
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<BorrowingInfo> searchBorrowingInfo(String borrowerName, String bookTitle)
			throws ClassNotFoundException, SQLException {
		ArrayList<BorrowingInfo> borrowingInfos = new ArrayList<>();
		ResultSet rs;
		String queryForBookTitle = " AND booktitle LIKE ?";
		String query = SEARCH_BORROW_INFO;
		if (!bookTitle.isEmpty()) {
			query += queryForBookTitle;
		}
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, "%"+borrowerName+"%");
		if (!bookTitle.isEmpty()) {
			stmt.setString(2, "%" + bookTitle + "%");
		}
		rs = stmt.executeQuery();
		while (rs.next()) {
			BorrowingInfo borrowingInfo = new BorrowingInfo();
			borrowingInfo.setInfoID(rs.getInt("infoid"));
			borrowingInfo.setBookID(rs.getInt("bookid"));
			borrowingInfo.setCopyID(rs.getInt("copyid"));
			borrowingInfo.setCompensation(rs.getDouble("compensation"));
			borrowingInfo.setBorrowerName(rs.getString("borrowername"));
			borrowingInfo.setBookTitle(rs.getString("booktitle"));
			borrowingInfo.setBorrowedDate(rs.getDate("borroweddate"));
			borrowingInfo.setLentDate(rs.getDate("lentdate"));
			borrowingInfo.setReturnDate(rs.getDate("returndate"));
			borrowingInfo.setReturned(rs.getBoolean("returned"));
			borrowingInfos.add(borrowingInfo);
		}
		closeConnection();
		return borrowingInfos;
	}

	/**
	 * The method is used to update borrowing information.
	 * 
	 * @param infoID
	 *            id of the borrowing info that need to update.
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void updateBorrowingInfo() throws SQLException, ClassNotFoundException {
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(UPDATE_INFO);
		stmt.setInt(1, bookID);
		stmt.setInt(2, copyID);
		stmt.setDouble(3, compensation);
		stmt.setString(4, borrowerName);
		stmt.setString(5, bookTitle);
		stmt.setDate(6, borrowedDate);
		stmt.setDate(7, lentDate);
		stmt.setDate(8, returnDate);
		stmt.setBoolean(9, returned);
		stmt.setInt(10, infoID);
		stmt.execute();
		closeConnection();
	}

	public int countNotReturnedInfo(String borrowerName) throws ClassNotFoundException, SQLException {
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(COUNT_NOT_RETURNED_INFO);
		stmt.setString(1, borrowerName);
		ResultSet rs = stmt.executeQuery();
		int count =0;
		while(rs.next()) {
			count = rs.getInt("count");
		}
		return count;
	}
	
	/**
	 * Generate the info id.
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private int generateID() throws ClassNotFoundException, SQLException {
		return countRecord("BorrowingInfo")+1;
	}
	
	/**
	 * @return the bookID
	 */
	public int getBookID() {
		return bookID;
	}

	/**
	 * @param bookID
	 *            the bookID to set
	 */
	public void setBookID(int bookID) {
		this.bookID = bookID;
	}

	/**
	 * @return the copyID
	 */
	public int getCopyID() {
		return copyID;
	}

	/**
	 * @param copyID
	 *            the copyID to set
	 */
	public void setCopyID(int copyID) {
		this.copyID = copyID;
	}

	/**
	 * @return the compensation
	 */
	public double getCompensation() {
		return compensation;
	}

	/**
	 * @param compensation
	 *            the compensation to set
	 */
	public void setCompensation(double compensation) {
		this.compensation = compensation;
	}

	/**
	 * @return the borrowerName
	 */
	public String getBorrowerName() {
		return borrowerName;
	}

	/**
	 * @param borrowerName
	 *            the borrowerName to set
	 */
	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	/**
	 * @return the bookTitle
	 */
	public String getBookTitle() {
		return bookTitle;
	}

	/**
	 * @param bookTitle
	 *            the bookTitle to set
	 */
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	/**
	 * @return the borrowedDate
	 */
	public Date getBorrowedDate() {
		return borrowedDate;
	}

	/**
	 * @param borrowedDate
	 *            the borrowedDate to set
	 */
	public void setBorrowedDate(Date borrowedDate) {
		this.borrowedDate = borrowedDate;
	}

	/**
	 * @return the lentDate
	 */
	public Date getLentDate() {
		return lentDate;
	}

	/**
	 * @param lentDate
	 *            the lentDate to set
	 */
	public void setLentDate(Date lentDate) {
		this.lentDate = lentDate;
	}

	/**
	 * @return the returnDate
	 */
	public Date getReturnDate() {
		return returnDate;
	}

	/**
	 * @param returnDate
	 *            the returnDate to set
	 */
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	/**
	 * @return the infoID
	 */
	public int getInfoID() {
		return infoID;
	}

	/**
	 * @param infoID
	 *            the infoID to set
	 */
	public void setInfoID(int infoID) {
		this.infoID = infoID;
	}
	
	/**
	 * @return the returned
	 */
	public boolean isReturned() {
		return returned;
	}

	/**
	 * @param returned the returned to set
	 */
	public void setReturned(boolean returned) {
		this.returned = returned;
	}
}
