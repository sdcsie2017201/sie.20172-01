/**
 * Tran Tuan Anh
 * Date: Apr 4, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import librarymanagementsystem.utils.DBUtils;

/**
 * This class is used to manage copy information.
 * 
 * @author gadfl
 *
 */
public class Copy extends DBUtils {

	private final String UPDATE_COPY = "UPDATE Copy SET Condition=?, Borrowed=?, Lent=? WHERE BookID=? AND CopyID=?";
	private final String GET_COPY = "SELECT * FROM Copy WHERE Bookid=? AND Copyid=?";
	private final String GET_AVAIL_COPY = "SELECT * FROM Copy WHERE BookID =? AND NOT Borrowed AND Type LIKE \"%Lent%\"";

	private int copyID;
	private int bookID;
	private String copyType;
	private double price;
	private int condition;
	private boolean isBorrowed;
	private boolean isLent;

	/**
	 * This method is used to update the current status of a copy.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void updateCopyStatus() throws ClassNotFoundException, SQLException {
		createConnection();
		PreparedStatement stmt = conn.prepareStatement(UPDATE_COPY);
		stmt.setInt(1, condition);
		stmt.setBoolean(2, isBorrowed);
		stmt.setBoolean(3, isLent);
		stmt.setInt(4, bookID);
		stmt.setInt(5, copyID);
		stmt.execute();
		closeConnection();
	}

	/**
	 * This method is used to get the copy from database.
	 * 
	 * @param bookID
	 *            ID of the book that the copy is belong to.
	 * @param copyID
	 *            ID of the copy you want to get.
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void getCopy(int bookID, int copyID) throws ClassNotFoundException, SQLException {
		createConnection();
		ResultSet rs;
		PreparedStatement stmt = conn.prepareStatement(GET_COPY);
		stmt.setInt(1, bookID);
		stmt.setInt(2, copyID);
		rs = stmt.executeQuery();
		while (rs.next()) {
			this.setBookID(rs.getInt("BookID"));
			this.setCopyID(rs.getInt("CopyID"));
			this.setCopyType(rs.getString("Type"));
			this.setPrice(rs.getDouble("Price"));
			this.setCondition(rs.getInt("Condition"));
			this.setBorrowed(rs.getBoolean("Borrowed"));
			this.setLent(rs.getBoolean("Lent"));
		}
		closeConnection();
	}
	
	/**
	 * Get the copy of the book that is available to borrow.
	 * @param bookID
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Copy getAvailCopy(int bookID) throws ClassNotFoundException, SQLException{
		createConnection();
		ResultSet rs;
		PreparedStatement stmt = conn.prepareStatement(GET_AVAIL_COPY);
		stmt.setInt(1, bookID);
		rs = stmt.executeQuery();
		Copy copy = null;
		if (rs.next()) {
			copy = new Copy();
			copy.setBookID(rs.getInt("BookID"));
			copy.setCopyID(rs.getInt("CopyID"));
			copy.setCopyType(rs.getString("Type"));
			copy.setPrice(rs.getDouble("Price"));
			copy.setCondition(rs.getInt("Condition"));
			copy.setBorrowed(rs.getBoolean("Borrowed"));
			copy.setLent(rs.getBoolean("Lent"));
		}
		closeConnection();
		return copy;
	}

	/**
	 * @return the copyID
	 */
	public int getCopyID() {
		return copyID;
	}

	/**
	 * @param copyID
	 *            the copyID to set
	 */
	public void setCopyID(int copyID) {
		this.copyID = copyID;
	}

	/**
	 * @return the bookID
	 */
	public int getBookID() {
		return bookID;
	}

	/**
	 * @param bookID
	 *            the bookID to set
	 */
	public void setBookID(int bookID) {
		this.bookID = bookID;
	}

	/**
	 * @return the copyType
	 */
	public String getCopyType() {
		return copyType;
	}

	/**
	 * @param copyType
	 *            the copyType to set
	 */
	public void setCopyType(String copyType) {
		this.copyType = copyType;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the condition
	 */
	public int getCondition() {
		return condition;
	}

	/**
	 * @param condition
	 *            the condition to set
	 */
	public void setCondition(int condition) {
		this.condition = condition;
	}

	/**
	 * @return the isBorrowed
	 */
	public boolean isBorrowed() {
		return isBorrowed;
	}

	/**
	 * @param isBorrowed
	 *            the isBorrowed to set
	 */
	public void setBorrowed(boolean isBorrowed) {
		this.isBorrowed = isBorrowed;
	}

	/**
	 * @return the isLent
	 */
	public boolean isLent() {
		return isLent;
	}

	/**
	 * @param isLent
	 *            the isLent to set
	 */
	public void setLent(boolean isLent) {
		this.isLent = isLent;
	}

}
