/**
 * Tran Tuan Anh
 * Date: May 1, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author gadfl
 *
 */
public class DateUtils {
	/**
	 * Convert a string to java.sql.Date.
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static java.sql.Date parseDate(String date) throws ParseException{
		SimpleDateFormat df = new SimpleDateFormat();
		Date parsedDate = df.parse(date);
		return new java.sql.Date(parsedDate.getTime());
	}
	
	/**
	 * Get the current date in java.sql.Date.
	 * @return
	 */
	public static java.sql.Date getCurrentDate(){
		return new java.sql.Date(Calendar.getInstance().getTime().getTime());
	}
	
	/**
	 * Add some days to the given date.
	 * @param date
	 * @param days
	 * @return
	 */
	public static java.sql.Date addDays(java.sql.Date date, int days){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		return new java.sql.Date(cal.getTimeInMillis());
	}
}
