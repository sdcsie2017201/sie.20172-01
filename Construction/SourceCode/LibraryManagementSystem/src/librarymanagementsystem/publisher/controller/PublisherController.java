/**
 * Tran Tuan Anh
 * Date: May 8, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.publisher.controller;

import java.sql.SQLException;

import librarymanagementsystem.entity.Publisher;

/**
 * @author gadfl
 *
 */
public class PublisherController {

	private static PublisherController pc;
	
	/**
	 * The constructor is private so that no external object can initializa object
	 * of this class.
	 */
	private PublisherController() {
		
	}
	
	/**
	 * This method is used to initialize a single shared object of BookController.
	 * @return
	 */
	public static PublisherController getInstance() {
		if(pc == null) {
			pc = new PublisherController();
		}
		return pc;
	}
	
	/**
	 * Add new publisher to database
	 * @param publisherName
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void add(String publisherName) throws ClassNotFoundException, SQLException {
		Publisher p = new Publisher();
		p.setPublisherName(publisherName);
		p.add();
	}
	
	/**
	 * Get all the publisher name from database.
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public String[] getAllPublisher() throws ClassNotFoundException, SQLException {
		Publisher p = new Publisher();
		return p.getAllPublisher();
	}
	
	/**
	 * Check if the pubisher is existed in database.
	 * @param publisherName
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean checkExistPublisher(String publisherName) throws ClassNotFoundException, SQLException {
		Publisher p = new Publisher();
		return p.checkExistPublisher(publisherName);
	}
}
