/**
 * Tran Tuan Anh
 * Date: Apr 18, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.borrowinginfo.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import librarymanagementsystem.book.gui.ReturnBookForm;
import librarymanagementsystem.borrowinginfo.controller.BorrowingController;
import librarymanagementsystem.common.MainForm;
import librarymanagementsystem.copy.controller.CopyController;
import librarymanagementsystem.entity.BorrowingInfo;
import librarymanagementsystem.entity.Copy;
import librarymanagementsystem.entity.User;
import librarymanagementsystem.user.controller.UserController;
import librarymanagementsystem.utils.DateUtils;

/**
 * @author gadfl
 *
 */
public class SearchBorrowingInfoForm extends JPanel{

	private JTextField borrowerNameField;
	private JTextField bookTitleField;
	private JTable borrowingInfoTable;

	/**
	 * Create the application.
	 */
	public SearchBorrowingInfoForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the 
	 */
	private void initialize() {
		setBounds(100, 100, 800, 600);
		
		borrowerNameField = new JTextField();
		borrowerNameField.setColumns(10);
		
		JLabel lblBorrowersName = new JLabel("Borrower's name");
		
		JLabel lblBookTitle = new JLabel("Book title");
		
		bookTitleField = new JTextField();
		bookTitleField.setColumns(10);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String borrowerName = borrowerNameField.getText();
				String bookTitle = bookTitleField.getText();
				try {
					searchBorrowingInfo(borrowerName, bookTitle);
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		
		borrowingInfoTable = new JTable();
		scrollPane.setViewportView(borrowingInfoTable);
		borrowingInfoTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"InfoID", "Book ID", "Copy ID", "Compensation", "BorrowerName", "BookTitle", "BorrowedDate", "LentDate", "ReturnDate", "Returned"
			}
		));
		
		JLabel lblBorrowingInformation = new JLabel("Borrowing Information");
		
		JButton btnReturn = new JButton("Return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BorrowingInfo bi = null;
				try {
					bi = getSelectedItem();
				} catch (ParseException e2) {
					e2.printStackTrace();
				}
				if(bi != null) {
					try {
						ReturnBookForm rb = new ReturnBookForm(bi);
						rb.display();
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else {
					JOptionPane.showMessageDialog(null, "Please select a borrowing information");
				}
			}
		});
		
		JButton btnLent = new JButton("Lent");
		btnLent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					lentBook();
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(12)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblBorrowersName, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(122)
							.addComponent(borrowerNameField, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)))
					.addGap(25)
					.addComponent(lblBookTitle, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
					.addGap(12)
					.addComponent(bookTitleField, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(30)
					.addComponent(lblBorrowingInformation, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
					.addGap(481)
					.addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(12)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(546)
					.addComponent(btnLent, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
					.addGap(12)
					.addComponent(btnReturn, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(13)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblBorrowersName, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(borrowerNameField, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblBookTitle, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(bookTitleField, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
					.addGap(13)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(lblBorrowingInformation))
						.addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
					.addGap(13)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 406, GroupLayout.PREFERRED_SIZE)
					.addGap(2)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnLent)
						.addComponent(btnReturn)))
		);
		setLayout(groupLayout);
	}
	
	private void lentBook() throws ParseException {
		BorrowingInfo bi = getSelectedItem();
		if(bi != null) {
			try {
				Copy copy = CopyController.getInstance().getCopy(bi.getBookID(), bi.getCopyID());
				int choice = JOptionPane.showConfirmDialog(null, "Lent the copy with ID :" + copy.getCopyID());
				if(choice == JOptionPane.YES_OPTION) {
					if(copy.isLent()) {
						JOptionPane.showMessageDialog(null, "The book is already lent.");
					}else {
						copy.setLent(true);
						copy.updateCopyStatus();
						bi.setLentDate(DateUtils.getCurrentDate());
						bi.updateBorrowingInfo();
					}
				}
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}else {
			JOptionPane.showMessageDialog(null, "Plase select a borrowing information.");
		}
	}
	
	private ArrayList<BorrowingInfo> borrowingInfoList = new ArrayList<>();
	private BorrowingController bc;
	
	/**Search the borrowing information
	 * @param borrowerName
	 * @param bookTitle
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	private void searchBorrowingInfo(String borrowerName, String bookTitle) throws ClassNotFoundException, SQLException {
		bc = BorrowingController.getInstance();
		borrowingInfoList.clear();
		borrowingInfoList = bc.searchBorrowingInfo(borrowerName, bookTitle);
		showBorrowingInfoTable();
	}
	
	/**
	 * Get the selected borrowing information.
	 * @return
	 * @throws ParseException 
	 */
	private BorrowingInfo getSelectedItem() throws ParseException {
		int row = borrowingInfoTable.getSelectedRow();
		if(row != -1) {
			BorrowingInfo bi = new BorrowingInfo();
			bi.setInfoID((int)borrowingInfoTable.getModel().getValueAt(row, 0));
			bi.setBookID((int)borrowingInfoTable.getModel().getValueAt(row, 1));
			bi.setCopyID((int)borrowingInfoTable.getModel().getValueAt(row, 2));
			bi.setCompensation((double)borrowingInfoTable.getModel().getValueAt(row,3));
			bi.setBorrowerName((String)borrowingInfoTable.getModel().getValueAt(row, 4));
			bi.setBookTitle((String)borrowingInfoTable.getModel().getValueAt(row, 5));
			bi.setBorrowedDate((java.sql.Date)borrowingInfoTable.getModel().getValueAt(row, 6));
			bi.setLentDate((java.sql.Date)borrowingInfoTable.getModel().getValueAt(row, 7));
			bi.setReturnDate((java.sql.Date)borrowingInfoTable.getModel().getValueAt(row, 8));
			bi.setReturned((boolean)borrowingInfoTable.getModel().getValueAt(row, 9));
			return bi;
		}else{
			return null;
		}
	}
	
	/**
	 * Display the borrowing information
	 */
	private void showBorrowingInfoTable() {
		DefaultTableModel model = (DefaultTableModel) borrowingInfoTable.getModel();
		model.setRowCount(0);
		Object[] rowData = new Object[10];
		for(BorrowingInfo bi : borrowingInfoList) {
			if(!bi.isReturned()) {
				rowData[0] = bi.getInfoID();
				rowData[1] = bi.getBookID();
				rowData[2] = bi.getCopyID();
				rowData[3] = bi.getCompensation();
				rowData[4] = bi.getBorrowerName();
				rowData[5] = bi.getBookTitle();
				rowData[6] = bi.getBorrowedDate();
				rowData[7] = bi.getLentDate();
				rowData[8] = bi.getReturnDate();
				rowData[9] = bi.isReturned();
				model.addRow(rowData);
			}
		}
	}
}
