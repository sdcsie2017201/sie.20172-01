/**
 * Tran Tuan Anh
 * Date: Apr 18, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.borrowinginfo.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import librarymanagementsystem.borrowinginfo.controller.BorrowingController;
import librarymanagementsystem.entity.BorrowingInfo;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

/**
 * @author gadfl
 *
 */
public class SearchReturnInfoForm extends JPanel{

	private JTextField borrowerNameField;
	private JTextField bookTitleField;
	private JTable borrowingInfoTable;

	/**
	 * Create the application.
	 */
	public SearchReturnInfoForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the 
	 */
	private void initialize() {
		setBounds(100, 100, 800, 600);
		
		borrowerNameField = new JTextField();
		borrowerNameField.setColumns(10);
		
		JLabel lblBorrowersName = new JLabel("Borrower's name");
		
		JLabel lblBookTitle = new JLabel("Book title");
		
		bookTitleField = new JTextField();
		bookTitleField.setColumns(10);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String borrowerName = borrowerNameField.getText();
				String bookTitle = bookTitleField.getText();
				try {
					searchBorrowingInfo(borrowerName, bookTitle);
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				showBorrowingInfoTable();
			}
		});
		
		JLabel lblBorrowingInformation = new JLabel("Borrowing Information");
		
		scrollPane = new JScrollPane();
		
		borrowingInfoTable = new JTable();
		scrollPane.setViewportView(borrowingInfoTable);
		borrowingInfoTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"InfoID", "Compensation", "BorrowerName", "BookTitle", "BorrowedDate", "LentDate", "ReturnDate", "Returned"
			}
		) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] {
				true, true, true, false, true, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(12)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(122)
							.addComponent(borrowerNameField, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE))
						.addComponent(lblBorrowersName, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE))
					.addGap(25)
					.addComponent(lblBookTitle, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
					.addGap(12)
					.addComponent(bookTitleField, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(655)
					.addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(30)
					.addComponent(lblBorrowingInformation, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(30)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 721, GroupLayout.PREFERRED_SIZE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(13)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(borrowerNameField, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblBorrowersName, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblBookTitle, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
						.addComponent(bookTitleField, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
					.addGap(13)
					.addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
					.addGap(17)
					.addComponent(lblBorrowingInformation)
					.addGap(13)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE))
		);
		setLayout(groupLayout);
	}
	
	private ArrayList<BorrowingInfo> borrowingInfoList = new ArrayList<>();
	private BorrowingController bc;
	private JScrollPane scrollPane;
	
	/**Search the borrowing information
	 * @param borrowerName
	 * @param bookTitle
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	private void searchBorrowingInfo(String borrowerName, String bookTitle) throws ClassNotFoundException, SQLException {
		bc = BorrowingController.getInstance();
		borrowingInfoList.clear();
		borrowingInfoList = bc.searchBorrowingInfo(borrowerName, bookTitle);
		System.out.println("111");
	}
	
	/**
	 * Display the borrowing information
	 */
	private void showBorrowingInfoTable() {
		DefaultTableModel model = (DefaultTableModel) borrowingInfoTable.getModel();
		model.setRowCount(0);
		Object[] rowData = new Object[9];
		for(int i =0; i < borrowingInfoList.size(); i++) {
			borrowingInfoList.get(i).isReturned();
			if(borrowingInfoList.get(i).isReturned()) {
				rowData[0] = borrowingInfoList.get(i).getInfoID();
				rowData[1] = borrowingInfoList.get(i).getCompensation();
				rowData[2] = borrowingInfoList.get(i).getBorrowerName();
				rowData[3] = borrowingInfoList.get(i).getBookTitle();
				rowData[4] = borrowingInfoList.get(i).getBorrowedDate();
				rowData[5] = borrowingInfoList.get(i).getLentDate();
				rowData[6] = borrowingInfoList.get(i).getReturnDate();
				rowData[8] = borrowingInfoList.get(i).isReturned();
				model.addRow(rowData);
			}
		}
	}
}
