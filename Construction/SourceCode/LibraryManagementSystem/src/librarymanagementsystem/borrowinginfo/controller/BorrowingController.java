/**
 * Tran Tuan Anh
 * Date: Apr 4, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.borrowinginfo.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import librarymanagementsystem.entity.BorrowingInfo;
import librarymanagementsystem.entity.Copy;

/**
 * This class control borrow or return book function;
 * 
 * @author gadfl
 *
 */
public class BorrowingController {

	private static BorrowingController bc;

	/**
	 * The constructor is private so that no external object can initializa object
	 * of this class.
	 */
	private BorrowingController() {
	}

	/**
	 * This method is used to initialize a single shared object of BookController.
	 * 
	 * @return the shared object.
	 */
	public static BorrowingController getInstance() {
		if (bc == null) {
			bc = new BorrowingController();
		}
		return bc;
	}

	/**
	 * This method is used to get the list of bororwing information belong to the
	 * given borrower's name;
	 * 
	 * @param borrowerName
	 *            name of the borrower.
	 * @param bookTitle
	 *            title of the book.
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<BorrowingInfo> searchBorrowingInfo(String borrowerName, String bookTitle)
			throws ClassNotFoundException, SQLException {
		BorrowingInfo borrowingInfo = new BorrowingInfo();
		return borrowingInfo.searchBorrowingInfo(borrowerName, bookTitle);
	}

	/**
	 * Add new borrowing information.
	 * @param bi
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void addBorrowingInfo(BorrowingInfo bi) throws ClassNotFoundException, SQLException {
		bi.addBorrowingInfo();
	}
	
	/**
	 * This method is used to get the list of copy has been borrowed by the
	 * borrower.
	 * 
	 * @param borrowerName
	 *            name of the borrower.
	 * @return a list of copy.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<Copy> initiateCopyList(String borrowerName) throws ClassNotFoundException, SQLException {
		ArrayList<BorrowingInfo> borrowingInfos = this.searchBorrowingInfo(borrowerName, "");
		ArrayList<Copy> copyList = new ArrayList<>();
		Copy copy = new Copy();
		for (BorrowingInfo borrowingInfo : borrowingInfos) {
			copy.getCopy(borrowingInfo.getBookID(), borrowingInfo.getCopyID());
			copyList.add(copy);
		}
		return copyList;
	}
	
	/**
	 * Count all borrwing info that is no returned.
	 * @param borrowerName
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int countNotReturnedInfo(String borrowerName) throws ClassNotFoundException, SQLException {
		BorrowingInfo bi = new BorrowingInfo();
		return bi.countNotReturnedInfo(borrowerName);
	}
}
