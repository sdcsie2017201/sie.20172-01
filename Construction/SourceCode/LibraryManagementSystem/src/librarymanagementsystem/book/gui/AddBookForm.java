/**
 * Tran Tuan Anh
 * Date: Apr 10, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.book.gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import librarymanagementsystem.book.controller.BookController;
import librarymanagementsystem.publisher.controller.PublisherController;

/**
 * @author gadfl
 *
 */
public class AddBookForm {

	private JFrame frame;
	private JTextField titleField;
	private JTextField authorField;
	private JTextField isbnField;

	private JLabel lblTitleError;
	private JLabel lblAuthorError;
	private JLabel lblIsbnError;

	private JComboBox<String> categoryBox;
	private JComboBox<String> publisherBox;

	private final String EMPTY_FIELD_MESSAGE = "This field cannot be empty";
	private final String INVALID_VALUE_MESSAGE = "The value is invalid";

	private final String[] BOOK_CATEGORY = { "Politics-Laws", "Science and Technology", "Literature", "Culture-Society",
			"Economy", "Children's Book", "Education" };
	private String[] bookPublisher;

	public void display() {
		if(frame != null) {
			frame.setVisible(true);
		}
	}
	
	/**
	 * Create the application.
	 */
	public AddBookForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 480, 640);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblTitle = new JLabel("Title");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblTitle.setBounds(38, 34, 65, 30);
		frame.getContentPane().add(lblTitle);

		titleField = new JTextField();
		titleField.setBounds(38, 77, 394, 40);
		frame.getContentPane().add(titleField);
		titleField.setColumns(10);

		JLabel lblNewLabel = new JLabel("Author");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel.setBounds(38, 123, 56, 16);
		frame.getContentPane().add(lblNewLabel);

		authorField = new JTextField();
		authorField.setBounds(38, 157, 394, 40);
		frame.getContentPane().add(authorField);
		authorField.setColumns(10);

		categoryBox = new JComboBox<>();
		categoryBox.setModel(new DefaultComboBoxModel<>(BOOK_CATEGORY));
		categoryBox.setBounds(48, 241, 186, 22);
		frame.getContentPane().add(categoryBox);

		JLabel lblNewLabel_1 = new JLabel("Category");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel_1.setBounds(38, 202, 128, 26);
		frame.getContentPane().add(lblNewLabel_1);

		JLabel lblPublisher = new JLabel("Publisher");
		lblPublisher.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblPublisher.setBounds(38, 276, 128, 22);
		frame.getContentPane().add(lblPublisher);

		try {
			bookPublisher = PublisherController.getInstance().getAllPublisher();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		publisherBox = new JComboBox<>();
		publisherBox.setModel(new DefaultComboBoxModel<>(bookPublisher));
		publisherBox.setBounds(48, 311, 245, 22);
		frame.getContentPane().add(publisherBox);

		JLabel lblIsbn = new JLabel("ISBN");
		lblIsbn.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblIsbn.setBounds(38, 346, 56, 16);
		frame.getContentPane().add(lblIsbn);

		isbnField = new JTextField();
		isbnField.setBounds(38, 375, 398, 40);
		frame.getContentPane().add(isbnField);
		isbnField.setColumns(10);

		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(38, 448, 170, 67);
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				addBook();
			}
		});
		frame.getContentPane().add(btnAdd);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(266, 448, 166, 67);
		frame.getContentPane().add(btnCancel);

		lblTitleError = new JLabel("");
		lblTitleError.setBounds(86, 43, 346, 16);
		frame.getContentPane().add(lblTitleError);

		lblAuthorError = new JLabel("");
		lblAuthorError.setBounds(106, 125, 326, 16);
		frame.getContentPane().add(lblAuthorError);

		lblIsbnError = new JLabel("");
		lblIsbnError.setBounds(86, 346, 346, 16);
		frame.getContentPane().add(lblIsbnError);
	}

	/**
	 * Check if all input is valid
	 * 
	 * @param title
	 *            title of the book (valid if it not empty)
	 * @param author
	 *            author of the book (valid if it not empty)
	 * @param isbn
	 *            a unique numeric commercial book identifier (valid if it 10 or 13
	 *            digits long and has no letter)
	 * @return
	 */
	public boolean checkValidInput(String title, String author, String isbn) {
		if (title.isEmpty()) {
			lblTitleError.setText(EMPTY_FIELD_MESSAGE);
			return false;
		}
		if (author.isEmpty()) {
			lblAuthorError.setText(EMPTY_FIELD_MESSAGE);
			return false;
		}
		System.out.println(isbn.length());
		if (isbn.length() == 10 || isbn.length() == 13) {
			try {
				Long.parseLong(isbn);
				return true;
			} catch (NumberFormatException e) {
				lblIsbnError.setText(INVALID_VALUE_MESSAGE);
				return false;
			}
		} else {
			lblIsbnError.setText("The length of the isbn must be 10 or 13");
			return false;
		}
	}

	/**
	 * Add new book to database.
	 */
	private void addBook() {
		String title = titleField.getText();
		String author = authorField.getText();
		String category = getCategory();
		String publisher = getPublisher();
		String isbn = isbnField.getText();
		if (checkValidInput(title, author, isbn)) {
			BookController controller = BookController.getInstance();
			try {
				controller.addBook(title, author, category, publisher, isbn);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Get the selected category.
	 * 
	 * @return
	 */
	private String getCategory() {
		int index = categoryBox.getSelectedIndex();
		return categoryBox.getModel().getElementAt(index);
	}

	/**
	 * Get the selected publisher.
	 * 
	 * @return
	 */
	private String getPublisher() {
		int index = publisherBox.getSelectedIndex();
		return publisherBox.getModel().getElementAt(index);
	}
}
