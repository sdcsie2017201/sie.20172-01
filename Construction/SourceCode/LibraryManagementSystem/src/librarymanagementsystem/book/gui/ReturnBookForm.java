/**
 * Tran Tuan Anh
 * Date: Apr 24, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.book.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import librarymanagementsystem.book.controller.BookController;
import librarymanagementsystem.copy.controller.CopyController;
import librarymanagementsystem.entity.Book;
import librarymanagementsystem.entity.BorrowingInfo;
import librarymanagementsystem.entity.Copy;
import librarymanagementsystem.utils.DateUtils;

/**
 * @author gadfl
 *
 */
public class ReturnBookForm{

	private JFrame frame;
	private JTextField conditionField;

//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					ReturnBox window = new ReturnBox();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	public void display() {
		if(frame != null) {
			frame.setVisible(true);
		}
	}
	
	private BorrowingInfo bInfo;
	private Copy copy;
	private Book book;
	
	/**
	 * Create the application.
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public ReturnBookForm(BorrowingInfo bInfo) throws ClassNotFoundException, SQLException {
		this.bInfo = bInfo;
		this.copy = CopyController.getInstance().getCopy(this.bInfo.getBookID(), this.bInfo.getCopyID());
		System.out.println(copy.getPrice());
		this.book = BookController.getInstance().getBookInformation(this.bInfo.getBookID());
		System.out.println(book.getBookTitle());
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 400);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(12, 29, 56, 16);
		frame.getContentPane().add(lblTitle);
		
		JLabel lblCopyId = new JLabel("Copy ID");
		lblCopyId.setBounds(12, 64, 56, 16);
		frame.getContentPane().add(lblCopyId);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(12, 103, 56, 16);
		frame.getContentPane().add(lblPrice);
		
		JLabel lblLentDate = new JLabel("Lent date");
		lblLentDate.setBounds(12, 143, 56, 16);
		frame.getContentPane().add(lblLentDate);
		
		JLabel lblCondition = new JLabel("Condition");
		lblCondition.setBounds(12, 185, 56, 16);
		frame.getContentPane().add(lblCondition);
		
		JButton btnReturn = new JButton("Return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					updateCopyInfo();
					updateBorrowingInfo();
					frame.dispose();
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		btnReturn.setBounds(12, 249, 97, 25);
		frame.getContentPane().add(btnReturn);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(143, 249, 97, 25);
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		frame.getContentPane().add(btnCancel);
		
		JLabel lblTitle_1 = new JLabel("");
		lblTitle_1.setBounds(94, 29, 357, 16);
		lblTitle_1.setText(book.getBookTitle());
		frame.getContentPane().add(lblTitle_1);
		
		JLabel lblCopyID_1 = new JLabel("");
		lblCopyID_1.setBounds(94, 64, 357, 16);
		lblCopyID_1.setText(String.valueOf(copy.getCopyID()));
		frame.getContentPane().add(lblCopyID_1);
		
		JLabel lblPrice_1 = new JLabel("");
		lblPrice_1.setBounds(94, 103, 357, 16);
		lblPrice_1.setText(String.valueOf(copy.getPrice()));
		frame.getContentPane().add(lblPrice_1);
		
		JLabel lblLentDate_1 = new JLabel("");
		lblLentDate_1.setBounds(94, 143, 357, 16);
		lblLentDate_1.setText(bInfo.getLentDate().toString());
		frame.getContentPane().add(lblLentDate_1);
		
		conditionField = new JTextField();
		conditionField.setBounds(94, 182, 82, 25);
		conditionField.setText(String.valueOf(copy.getCondition()));
		frame.getContentPane().add(conditionField);
		conditionField.setColumns(10);
		
		JLabel label = new JLabel("%");
		label.setBounds(188, 185, 56, 16);
		frame.getContentPane().add(label);
	}
	
	/**
	 * @throws NumberFormatException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private void updateCopyInfo() throws NumberFormatException, ClassNotFoundException, SQLException {
		CopyController.getInstance().updateCopyStatus(Integer.parseInt(conditionField.getText()), false, false);
	}
	
	/**
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws NumberFormatException
	 */
	private void updateBorrowingInfo() throws ClassNotFoundException, SQLException, NumberFormatException{
		bInfo.setReturnDate(DateUtils.getCurrentDate());
		bInfo.setCompensation(calCompensation());
		bInfo.setReturned(true);
		bInfo.updateBorrowingInfo();
	}
	
	/**
	 * @return
	 * @throws NumberFormatException
	 */
	private double calCompensation() throws NumberFormatException{
		double com = 0;
		Date lentDate = bInfo.getLentDate();
		Date returnDate = DateUtils.getCurrentDate();
		long overdue = TimeUnit.DAYS.convert(returnDate.getTime()-lentDate.getTime(), TimeUnit.MILLISECONDS);
		
		if(overdue != 0) {
			com += (-1)*overdue*5000;
		}
		
		int oldStatus = copy.getCondition();
		int newStatus = Integer.parseInt(conditionField.getText());
		
		int diff = oldStatus-newStatus;
		if(diff > 0) {
			com += diff*copy.getPrice();
		}
		
		return com;
	}
	
}
