/**
 * Nguyen Tuan Anh
 * Date: Apr 29, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.book.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import librarymanagementsystem.book.controller.BookController;
import librarymanagementsystem.borrowinginfo.controller.BorrowingController;
import librarymanagementsystem.common.MainForm;
import librarymanagementsystem.entity.Book;
import librarymanagementsystem.entity.Role;
import librarymanagementsystem.entity.User;
import librarymanagementsystem.user.controller.UserController;

/**
 * @author tuananh
 *
 */
public class SearchBookForm extends JPanel {

	private JTextField titleField;
	private JTextField authorField;
	private JTable bookTable;

	private ArrayList<Book> bookCart;
	private ArrayList<Book> bookList;

	/**
	 * Create the application.
	 * 
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public SearchBookForm() throws ClassNotFoundException, SQLException {
		initialize();
	}

	/**
	 * Initialize the contents of the
	 * 
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	private void initialize() throws ClassNotFoundException, SQLException {
		Role role = new Role();
		String roleId = role.getRoleIdsByUser(MainForm.getEmail());

		
		bookCart = new ArrayList<>();
		setBounds(0, 0, 800, 600);

		JLabel lblTitle = new JLabel("Title");

		titleField = new JTextField();
		titleField.setColumns(10);

		JLabel lblAuthor = new JLabel("Author");

		authorField = new JTextField();
		authorField.setColumns(10);

		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// bookTable.removeAll();
				String title = titleField.getText();
				String author = authorField.getText();
				try {
					bookList = BookController.getInstance().searchBook(title, author);
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				showSearchResult();
			}
		});

		JLabel lblBookList = new JLabel("Book list");

		JLabel lblItemCount = new JLabel("Item count:");

		JLabel lblCount = new JLabel("");

		JButton btnAddToCart = new JButton("Add to cart");
		btnAddToCart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					User user = UserController.getInstance().findUserByEmail(MainForm.getEmail());
					String name = user.getLastName() +" "+user.getFirstName();
					int limit = 5 - BorrowingController.getInstance().countNotReturnedInfo(name);
					if(bookCart.size() < limit) {
						Book selectedBook = getSelectedItem();
						if(!bookCart.contains(selectedBook)) {
							bookCart.add(getSelectedItem());
							lblCount.setText(String.valueOf(bookCart.size()));
						}else {
							JOptionPane.showMessageDialog(null, "Sorry! You can borrow only one copy of one book.");
						}
					}else {
						JOptionPane.showMessageDialog(null, "The number of book you borrowed have reach the limit. Return some to borrow more.");
					}
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		JButton btnViewCart = new JButton("View cart");
		btnViewCart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CartViewForm cartWindow = new CartViewForm(bookCart);
				cartWindow.display();
			}
		});

		JButton btnAddBook = new JButton("Add book");
		btnAddBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddBookForm abf = new AddBookForm();
				abf.display();
			}
		});

		if (roleId.equals("2")) {
			lblItemCount.setVisible(true);
			lblCount.setVisible(true);
			btnAddToCart.setVisible(true);
			btnViewCart.setVisible(true);
			btnAddBook.setVisible(false);
		} else {
			lblItemCount.setVisible(false);
			lblCount.setVisible(false);
			btnAddToCart.setVisible(false);
			btnViewCart.setVisible(false);
			btnAddBook.setVisible(true);
		}

		JScrollPane scrollPane = new JScrollPane();
		bookTable = new JTable();
		scrollPane.setViewportView(bookTable);
		bookTable.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "ID", "Title", "Author", "Category", "Publisher", "ISBN" }));
		
		showBookData();

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addGap(12)
						.addComponent(lblTitle, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE).addGap(12)
						.addComponent(titleField, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
						.addGap(56).addComponent(lblAuthor, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
						.addGap(12)
						.addComponent(authorField, GroupLayout.PREFERRED_SIZE, 294, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup().addGap(12)
						.addComponent(lblBookList, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
						.addGap(573)
						.addComponent(btnSearch, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup().addGap(12).addComponent(scrollPane,
						GroupLayout.PREFERRED_SIZE, 758, GroupLayout.PREFERRED_SIZE))
				.addGroup(groupLayout.createSequentialGroup().addGap(318).addComponent(btnAddBook)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addGap(73).addComponent(lblCount,
										GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblItemCount, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
						.addGap(8)
						.addComponent(btnAddToCart, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
						.addGap(12)
						.addComponent(btnViewCart, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addGap(10)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addGap(3).addComponent(lblTitle))
								.addComponent(titleField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup().addGap(3).addComponent(lblAuthor))
								.addComponent(authorField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addGap(13)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addGap(15).addComponent(lblBookList))
								.addComponent(btnSearch))
						.addGap(10)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 416, GroupLayout.PREFERRED_SIZE).addGap(9)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addGap(4)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lblCount, GroupLayout.PREFERRED_SIZE, 16,
														GroupLayout.PREFERRED_SIZE)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(lblItemCount).addComponent(btnAddBook))))
								.addComponent(btnAddToCart).addComponent(btnViewCart))));
		setLayout(groupLayout);
	}

	/**
	 * Get the selected book from the table;
	 * 
	 * @return
	 */
	private Book getSelectedItem() {
		int row = bookTable.getSelectedRow();
		Book book = new Book();
		book.setBookID((int) bookTable.getValueAt(row, 0));
		book.setBookTitle((String) bookTable.getModel().getValueAt(row, 1));
		book.setBookAuthor((String) bookTable.getModel().getValueAt(row, 2));
		book.setBookCategory((String) bookTable.getModel().getValueAt(row, 3));
		book.setBookPublisher((String) bookTable.getModel().getValueAt(row, 4));
		book.setIsbn((String) bookTable.getModel().getValueAt(row, 5));
		return book;
	}

	/**
	 * Show the search result
	 */
	private void showSearchResult() {
		DefaultTableModel model = (DefaultTableModel) bookTable.getModel();
		model.setRowCount(0);
		Object[] rowData = new Object[6];
		for (int i = 0; i < bookList.size(); i++) {
			rowData[0] = bookList.get(i).getBookID();
			rowData[1] = bookList.get(i).getBookTitle();
			rowData[2] = bookList.get(i).getBookAuthor();
			rowData[3] = bookList.get(i).getBookCategory();
			rowData[4] = bookList.get(i).getBookPublisher();
			rowData[5] = bookList.get(i).getIsbn();
			model.addRow(rowData);
		}
	}
	
	/**
	 * Show all book from database
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private void showBookData() throws ClassNotFoundException, SQLException{
		DefaultTableModel model = (DefaultTableModel) bookTable.getModel();
		model.setRowCount(0);
		ArrayList<Book> list = BookController.getInstance().getAllBook();
		Object[] rowData = new Object[6];
		for (int i = 0; i <list.size(); i++) {
			rowData[0] = list.get(i).getBookID();
			rowData[1] = list.get(i).getBookTitle();
			rowData[2] = list.get(i).getBookAuthor();
			rowData[3] = list.get(i).getBookCategory();
			rowData[4] = list.get(i).getBookPublisher();
			rowData[5] = list.get(i).getIsbn();
			model.addRow(rowData);
		}
	}
}
