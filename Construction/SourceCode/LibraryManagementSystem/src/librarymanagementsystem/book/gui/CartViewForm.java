/**
 * Tran Tuan Anh
 * Date: May 1, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.book.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import librarymanagementsystem.borrowinginfo.controller.BorrowingController;
import librarymanagementsystem.common.MainForm;
import librarymanagementsystem.copy.controller.CopyController;
import librarymanagementsystem.entity.Book;
import librarymanagementsystem.entity.BorrowingInfo;
import librarymanagementsystem.entity.Copy;
import librarymanagementsystem.entity.User;
import librarymanagementsystem.user.controller.UserController;
import librarymanagementsystem.utils.DateUtils;
import javax.swing.JScrollPane;

/**
 * @author gadfl
 *
 */
public class CartViewForm {

	private JFrame frame;
	private JTable table;

	private ArrayList<Book> bookList;
	private JScrollPane scrollPane;

	/**
	 * Create the application.
	 */
	public CartViewForm(ArrayList<Book> bookList) {
		this.bookList = bookList;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 340);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnRegisterToBorrow = new JButton("Register to borrow");
		btnRegisterToBorrow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					registerToBorrow();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnRegisterToBorrow.setBounds(431, 255, 139, 25);
		frame.getContentPane().add(btnRegisterToBorrow);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 13, 558, 229);
		frame.getContentPane().add(scrollPane);
		
				table = new JTable();
				scrollPane.setViewportView(table);
				table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "ID", "Title", "Author", "Publisher" }));
		
		showData();
	}
	
	/**
	 * Register to borrow the book.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private void registerToBorrow() throws ClassNotFoundException, SQLException {
			for(Book book: bookList) {
				User user = UserController.getInstance().findUserByEmail(MainForm.getEmail());
				String name = user.getLastName() +" "+ user.getFirstName();
				ArrayList<BorrowingInfo> bil = BorrowingController.getInstance().searchBorrowingInfo(name, book.getBookTitle());
				if(!bil.isEmpty()) {
					for(BorrowingInfo bi: bil) {
						if(!bi.isReturned()) {
							JOptionPane.showMessageDialog(null, "You already borrowed the book " + book.getBookTitle());
							return ;
						}
					}
				}else {
					Copy copy = CopyController.getInstance().getAvailCopy(book.getBookID());
					if(copy.getCopyID() == 0) {
						JOptionPane.showMessageDialog(null, "Sorry! All copy of"+book.getBookTitle()+"have been borrowed");
						return ;
					}else {
						addBorrowingInfo(book, copy);
						copy.setBorrowed(true);
						copy.updateCopyStatus();
					}
				}
			}
	}

	/**
	 * Show the book list to the table
	 */
	private void showData() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		Object[] rowData = new Object[4];
		for (int i = 0; i < bookList.size(); i++) {
			rowData[0] = bookList.get(i).getBookID();
			rowData[1] = bookList.get(i).getBookTitle();
			rowData[2] = bookList.get(i).getBookAuthor();
			rowData[3] = bookList.get(i).getBookPublisher();	
			model.addRow(rowData);
		}
	}
	
	private void addBorrowingInfo(Book book, Copy copy) throws ClassNotFoundException, SQLException {
		BorrowingInfo bi = new BorrowingInfo();
		User user = UserController.getInstance().findUserByEmail(MainForm.getEmail());
		String name = user.getLastName() +" "+ user.getFirstName();
		bi.setBookID(book.getBookID());
		bi.setCopyID(copy.getCopyID());
		bi.setBookTitle(book.getBookTitle());
		bi.setBorrowerName(name);
		bi.setCompensation(0);
		bi.setBorrowedDate(DateUtils.getCurrentDate());
		bi.setReturnDate(DateUtils.addDays(DateUtils.getCurrentDate(), 30));
		bi.setReturned(false);
		bi.addBorrowingInfo();
	}
	
	/**
	 * Display the window.
	 */
	public void display() {
		this.frame.setVisible(true);
	}
}
