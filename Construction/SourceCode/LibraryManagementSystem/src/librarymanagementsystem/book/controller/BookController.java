/*
 * Tran Tuan Anh
 * Date: Apr 3, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.book.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import librarymanagementsystem.entity.Book;

/**
 * This class control functions related to book
 * 
 * @author gadfl
 *
 */
public class BookController {

	private static BookController bc;

	/**
	 * The constructor is private so that no external object can initialize object
	 * of this class.
	 */
	private BookController() {
	}

	/**
	 * This method is used to initialize a single shared object of BookController.
	 * 
	 * @return the shared object.
	 */
	public static BookController getInstance() {
		if (bc == null) {
			bc = new BookController();
		}
		return bc;
	}

	/**
	 * This method is used to add new book to the book table.
	 * 
	 * @param title
	 * @param author
	 * @param publisher
	 * @param isbn
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	public void addBook(String title, String author, String category, String publisher, String isbn) throws SQLException, ClassNotFoundException {
		Book book = new Book();
		book.setBookTitle(title);
		book.setBookAuthor(author);
		book.setBookCategory(category);
		book.setBookPublisher(publisher);
		book.setIsbn(isbn);
		book.addBook();
	}

	/**
	 * @param bookID
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Book getBookInformation(int bookID) throws ClassNotFoundException, SQLException {
		Book book = new Book();
		book.setBookID(bookID);
		book.getInformation();
		return book;
	}
	
	/**
	 * Search the book in database.
	 * @param title
	 * @param author
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<Book> searchBook(String title, String author) throws ClassNotFoundException, SQLException{
		Book book = new Book();
		return book.searchBook(title, author);
	}
	
	/**
	 * Get All the book from database.
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<Book> getAllBook() throws ClassNotFoundException, SQLException{
		Book book = new Book();
		return book.getAllBook();
	}
	
}
