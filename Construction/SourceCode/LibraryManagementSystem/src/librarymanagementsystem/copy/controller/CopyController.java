/**
 * Tran Tuan Anh
 * Date: Apr 25, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.copy.controller;

import java.sql.SQLException;

import librarymanagementsystem.entity.Copy;

/**This class controll functions related to book.
 * @author gadfl
 *
 */
public class CopyController {

	private static CopyController cc;
	private static Copy copy;
	
	/**
	 * This constructor is private so that no external object can initialize object of this class.
	 */
	private CopyController() {
		copy = new Copy();
	}
	
	/**
	 * Initialize a single shared object of CopyController.
	 * @return
	 */
	public static CopyController getInstance() {
		if(cc == null) {
			cc = new CopyController();
		}
		return cc;
	}
	
	/**
	 * Get the copy with the given bookId and copyId.
	 * @param bookID
	 * @param copyID
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Copy getCopy(int bookID, int copyID) throws ClassNotFoundException, SQLException {
		Copy copy = new Copy();
		copy.getCopy(bookID, copyID);
		return copy;
	}
	
	/**
	 * Get the copy of the book that is available to borrow.
	 * @param bookID
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Copy getAvailCopy(int bookID) throws ClassNotFoundException, SQLException{
		return copy.getAvailCopy(bookID);
	}
	
	/**
	 * Update status of the copy.
	 * 
	 * @param bookID
	 *            ID of the book that the copy belong to.
	 * @param copyID
	 *            ID of the copy.
	 * @param condition
	 *            current condition of the copy.
	 * @param isBorrowed
	 *            the copy is borrowed but not lend physically to the borrower.
	 * @param isLent
	 *            the copy is lent physically to the borrower.
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void updateCopyStatus(int condition, boolean isBorrowed, boolean isLent)
			throws ClassNotFoundException, SQLException {
		Copy copy = new Copy();
		copy.setCondition(condition);
		copy.setBorrowed(isBorrowed);
		copy.setLent(isLent);
		copy.updateCopyStatus();
	}
}
