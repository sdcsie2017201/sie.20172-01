/**
 * Tran Tuan Anh
 * Date: Apr 7, 2018
 * Project: LibraryManagementSystem
 * Professor: Nguyen Thu Trang
 * Class: VUWIT-15
 */
package librarymanagementsystem.unittest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import librarymanagementsystem.book.gui.AddBookForm;

/**
 * @author gadfl
 *
 */
public class AddBookTest {

	private AddBookForm addBookGui;

	/**
	 * This method is used to set up the test
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		addBookGui = new AddBookForm();
	}

	/**
	 * This method is used to test checkValidField function.
	 * 
	 */
	@Test
	public void testCheckValidField() {
		//Title and author is empty, ISBN is invalid
		assertFalse(addBookGui.checkValidInput("", "", "B12345678"));
		assertFalse(addBookGui.checkValidInput("", "", "14785458910"));
		assertFalse(addBookGui.checkValidInput("", "", "145878912R"));
		//ISBN is valid,Title and author is empty
		assertFalse(addBookGui.checkValidInput("", "", "1231236547958"));
		//ISBN is valid, Title is empty 
		assertFalse(addBookGui.checkValidInput("", "Pham Huy Hoang", "3365478549"));
		//ISBN is valid, Author is empty
		assertFalse(addBookGui.checkValidInput("Code dao ki su", "", "1454411247124"));
		//ISBN is invalid , Title is empty
		assertFalse(addBookGui.checkValidInput("", "Stephen Hawking", "A1212224"));
		assertFalse(addBookGui.checkValidInput("", "Nguyen Hong", "12112124214"));
		assertFalse(addBookGui.checkValidInput("", "Thien Ha Ba Xuong", "121415123A"));
		//ISBN is invalid, Author is empty
		assertFalse(addBookGui.checkValidInput("Nhung ngay tho au", "", "7451248591AA"));
		assertFalse(addBookGui.checkValidInput("Vu tru trong hat de", "", "134574859112344"));
		assertFalse(addBookGui.checkValidInput("Me Tong Chi Quoc", "", "1234579A123469AB1"));
		//ISBN is invalid, Author and title is not empty
		assertFalse(addBookGui.checkValidInput("Nhung ngay tho au", "Nguyen Hong", "7451248591AA"));
		assertFalse(addBookGui.checkValidInput("Vu tru trong hat de", "Stephen Hawking", "134574859112344"));
		assertFalse(addBookGui.checkValidInput("Me Tong Chi Quoc", "Thien Ha Ba Xuong", "1234579A123469AB1"));
		//Title and author is not empty, ISBN is valid
		assertTrue(addBookGui.checkValidInput("Nhung ngay tho au", "Nguyen Hong", "9786046976899"));
		
	}

	/**
	 * This method is used to clear everything after the test
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		addBookGui = null;
	}

}
