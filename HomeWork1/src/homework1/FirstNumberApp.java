package homework1;

import java.util.Scanner;

public class FirstNumberApp {

	public static void main(String [] args) {
		Scanner sc = new Scanner(System.in);
		boolean running = true;
		int number = 0;
		
		while(running) {
			try {
				System.out.print("Enter a number: ");
				number = Integer.parseInt(sc.nextLine());
				running = false;
			}catch(Exception e) {
				System.out.println("You must enter a number.");
			}
		}
		
		System.out.println("Is prime number: " + NumberUtil.checkPrimeNumber(number));
		System.out.println("Is square number: " + NumberUtil.checkSquareNumber(number));
		System.out.println("Is perfect number: " + NumberUtil.checkPerfectNumber(number));
		
		sc.close();
	}
	
}
