package homework1;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

public class DateUtil {

	public static boolean checkDate(int day, int month, int year) {
		String date = String.format("%d-%d-%d"
				,day
				,month
				,year);//make a string date with format: "dd-mm-yyyy"
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd-mm-yyyy");
			df.setLenient(false);
			df.parse(date);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public static int calculateAge(int year) {
		int now = LocalDateTime.now().getYear();
		return now - year;
	}
	
}
