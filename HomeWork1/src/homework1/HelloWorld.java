package homework1;

import java.util.Scanner;

public class HelloWorld {

	public static void main(String [] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter your name: ");
		String name = sc.nextLine();
		System.out.println("Enter your birthday");
		System.out.print("Day: ");
		int day = Integer.parseInt(sc.nextLine());
		System.out.print("Month: ");
		int month = Integer.parseInt(sc.nextLine());
		System.out.print("Year: ");
		int year = Integer.parseInt(sc.nextLine());
		
		if(DateUtil.checkDate(day, month, year)) {
			System.out.println(String.format("Hi %s! Your age is: %d"
					,name
					,DateUtil.calculateAge(year)));
		}
		sc.close();
	}
	
}
