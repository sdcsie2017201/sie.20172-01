package homework1;

public class NumberUtil {

	public static boolean checkPrimeNumber(int number) {
		boolean isPrimeNumber = false;
		for(int i=1; i < Math.sqrt(number); i++) {
			if(!(number % i ==0)) {
				isPrimeNumber = true;
			}
		}
		return isPrimeNumber;
	}
	
	public static boolean checkSquareNumber(int number) {
		boolean isSquareNumber = false;
		if(number < 0) {
			return false;
		}else {
			int sqrt = (int)Math.sqrt(number);
			if(number == sqrt*sqrt) {
				isSquareNumber = true;
			}
		}
		return isSquareNumber;
	}
	
	public static boolean checkPerfectNumber(int number) {
		boolean isPerfectNumber = false;
		int sum = 0;
		
		if(number < 0) {
			return false;
		}else {
			for(int i=1; i< number; i++) {
				if(number%i==0) {
					sum+=i;
				}
			}
		}
		if(sum==number) {isPerfectNumber = true;}
		
		return isPerfectNumber;
	}
	
}
