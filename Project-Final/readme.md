Nhóm SIE.20172.01

Phân công chung xuyên suốt học kỳ 1. 20168033-Trần Tuấn Anh: Nhận trả sách, Tìm kiếm thông tin trả, Thêm sách. 2. 20168620-Phạm Thế Anh: Cho mượn sách, Tìm kiếm thông tin mượn, Thêm bản sao 3. 20168034-Trịnh Quang Anh: Đăng ký mượn sách, Xem danh sách mượn sách, Huỷ đăng ký mượn sách 4. 20168026-Nguyễn Tuấn Anh: Tìm kiếm sách, Phát hành thẻ bạn đọc, Cập nhật thông tin thẻ bạn đọc.

Phần Source Code được lưu tại Construction/SourceCode. Ngoài ra có một bản Zip của code ở Project-final/SoureCode