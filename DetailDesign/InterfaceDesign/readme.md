Nhóm SIE.20172.01

Phân công chung xuyên suốt học kỳ 1. 20168033-Trần Tuấn Anh: Nhận trả sách, Tìm kiếm thông tin trả, Thêm sách. 
2. 20168620-Phạm Thế Anh: Cho mượn sách, Tìm kiếm thông tin mượn, Thêm bản sao 
3. 20168034-Trịnh Quang Anh: Đăng ký mượn sách, Xem danh sách mượn sách, Huỷ đăng ký mượn sách 
4. 20168026-Nguyễn Tuấn Anh: Tìm kiếm sách, Phát hành thẻ bạn đọc, Cập nhật thông tin thẻ bạn đọc.

Phát sinh riêng trong tuần: 1. Trần Tuấn Anh: Merge cho cả nhóm 2. Trịnh Quang Anh: Không tham gia 3. Nguyễn Tuấn Anh: Vẽ biểu đồ dịch chuyển màn hình cho cả nhóm (sau khi cả nhóm đã trao đổi và thống nhất)

Kết quả review: Không review.